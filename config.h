// MQTT Settings
#define NETWORK_HOSTNAME "Office-Sonoff"
#define MQTT_HOST "192.168.1.160"
#define MQTT_PORT 1883
#define MQTT_USERNAME ""
#define MQTT_PASSWORD ""
#define MQTT_SUB_TOPIC "Office/events"

// Pin Definiton
#define SWITCH_PIN 0
#define LED_PIN 13
#define RELAY_PIN 12
