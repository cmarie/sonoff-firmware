#include <ArduinoJson.h>
#include "config.h"
#include "wifi.h"
#include "mqtt.h"

WIFI wifi;
MQTT mqtt;

void setup()
{
    // Debugging
    Serial.begin(115200);
    
    // Define Pin Modes
    pinMode(SWITCH_PIN, INPUT_PULLUP);
    pinMode(LED_PIN, OUTPUT);
    pinMode(RELAY_PIN, OUTPUT);
    
    // Set Relay and LED Off
    digitalWrite(LED_PIN, HIGH);
    digitalWrite(RELAY_PIN, HIGH);
    
    // Blink a few times before getting started to verify firmware was successful
    for (int i = 0; i < 10; i++)
    {
        digitalWrite(LED_PIN, !digitalRead(LED_PIN));
        delay(500);
    }
    // Setup WiFi with SmartConfig
    wifi.setup();
    
    // Setup MQTT
    mqtt.setup(mqttCallback);
}

void mqttCallback(char *topic, byte *payload, unsigned int length)
{
    DynamicJsonBuffer jsonBuffer(200);
    JsonObject &data = jsonBuffer.parseObject(payload);
    if (data.success())
    {
        bool state = data["on"];
        digitalWrite(LED_PIN, !state);
        digitalWrite(RELAY_PIN, !state);
    }
}

void loop()
{
    // Pressing the button will restart the Sonoff
    if (!digitalRead(SWITCH_PIN))
    {
        ESP.restart();
    }
    wifi.run();
    mqtt.run();
}

