#include "mqtt.h"
void MQTT::setup(CALLBACK_ f)
{
    client.setClient(espClient);
    client.setServer(MQTT_HOST , MQTT_PORT);
    client.setCallback(f);
    if(!connected()){
      Serial.println("Unable to connect to MQTT Server....");
    }
}
void MQTT::connect()
{
    if (client.connect(NETWORK_HOSTNAME, MQTT_USERNAME, MQTT_PASSWORD))
    {
        client.subscribe(MQTT_SUB_TOPIC);
    }
}
boolean MQTT::connected()
{
    return client.connected();
}
boolean MQTT::publish(const char *topic, const char *payload)
{
    bool callback = false;
    if (connected())
    {
        callback = client.publish(topic, (const uint8_t *)payload, strlen(payload), false);
    }
    return callback;
}

boolean MQTT::publish(const char *topic, const char *payload, boolean retained)
{
    bool callback = client.publish(topic, (const uint8_t *)payload, strlen(payload), retained);
    return callback;
}

boolean MQTT::publish(const char *topic, const uint8_t *payload, unsigned int plength)
{
    return client.publish(topic, payload, plength);
}

boolean MQTT::publish(const char *topic, const uint8_t *payload, unsigned int plength, boolean retained)
{
    return client.publish(topic, payload, plength, retained);
}
void MQTT::run()
{
    if (!client.connected())
    {
        connect();
    }
    client.loop();
}

