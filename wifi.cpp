#include "wifi.h"
WIFI::WIFI()
{
    WiFi.mode(WIFI_STA);
    WiFi.setSleepMode(WIFI_NONE_SLEEP);
}
WIFI::~WIFI() {}
void WIFI::setup()
{
    int cnt = 0;
    while (WiFi.status() != WL_CONNECTED)
    {
        yield();
        delay(500);
        Serial.print(".");
        if (cnt++ >= 30)
        {
            WiFi.beginSmartConfig();
            Serial.println("Starting Smart Config...");
            while (1)
            {
                yield();
                delay(1000);
                Serial.print("|");
                if (WiFi.smartConfigDone())
                {
                    break;
                }
                else if (cnt++ >= 60)
                {
                    ESP.restart();
                }
            }
        }
    }
    
}
void WIFI::run()
{
    if (WiFi.status() != WL_CONNECTED)
    {
        setup();
    }
}
