#ifndef WIFI_H_
#define WIFI_H_

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>

class WIFI
{
  public:
    WIFI();
    ~WIFI();
    void setup();
    void run();
};
#endif /* WIFI_H_ */

